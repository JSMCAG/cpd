#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define RECURSIVE 0
#define ITERATIVE 1
#define ANTI_DIAG 0

#define HIGH_COST 1

#define DEBUG 0
#define TIME 0

char *aString, *bString, *sequence;
short *resultMatrix;
int aSize, bSize;

short maxNumber(short a, short b) {
    if(a >= b) {
        return a;
    }

    return b;
}

#if HIGH_COST
/* used to make this problem more computationally difficult than it really is *
 * call this using a variable, and not a constant!                            */
short cost(int x) {
    int i, n_iter = 20;
    double dcost = 0;
    for(i = 0; i < n_iter; i++)
        dcost += pow(sin((double) x),2) + pow(cos((double) x),2);
    return (short) (dcost / n_iter + 0.1);
}
#else
short cost(int x) {
    return 1;
}
#endif


void saveString(char *destString, FILE *fp, int size) {
    int i;

    for(i = 0; i < size; i++) {
        fscanf(fp, "%c", &(destString[i]));
    }
    fscanf(fp, "\n");

    /* terminate string with \0 */
    destString[size] = '\0';
}

void printResultMatrix(FILE *stream) {
    unsigned int i, j;
    for(i = 0; i < (aSize + 1); i++) {
        fprintf(stream, "\t");
        for(j = 0; j < (bSize + 1); j++) {
            fprintf(stream, "%3d|", resultMatrix[i * (bSize + 1) + j]);
        }
        fprintf(stream, "\n");
    }
}

void initResultMatrix() {
    unsigned int i, j;
    /* allocate memory the result matrix */
    /* resultMatrix has one extra line and one extra column */
    resultMatrix = (short *) malloc ((aSize + 1) * (bSize + 1) * sizeof(short));


    /* Initialize first row and column of resultMatrix with 0 */
    for (j = 0; j < (bSize + 1); j++) {
        resultMatrix[j] = 0;
    }
    for (i = 1; i < (aSize + 1); i++) {
        resultMatrix[i * (bSize + 1)] = 0;
    }
    /* Initialize all other cells with -1, meaning it has not yet been calculated */
    for(i = 1; i < (aSize + 1); i++) {
        for(j = 1; j < (bSize + 1); j++) {
            resultMatrix[i * (bSize + 1) + j] = -1;
        }
    }


}

short getResultMatrix(unsigned int aPos, unsigned int bPos) {
    return resultMatrix[aPos * (bSize + 1) + bPos];
}

void setResultMatrix(unsigned int aPos, unsigned int bPos, short value) {
    resultMatrix[aPos * (bSize + 1) + bPos] = value;
}

void freeResultMatrix() {
    free(resultMatrix);
}

int lcs_anti_diagonal() {
    int aPos, bPos, aStart = 1, bStart = 1, i;

    while(bStart < (bSize + 1)) {

        for(i = 0; (aStart - i) > 0 && (bStart + i) < (bSize + 1); i++) {
            aPos = aStart - i;
            bPos = bStart + i;

            if (aString[aPos - 1] == bString[bPos - 1]) {
                setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
            } else {
                setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), getResultMatrix(aPos, bPos - 1)));
            }
        }

        if(aStart == aSize) {
            bStart++;
        } else {
            aStart++;
        }
    }

    return getResultMatrix(aSize, bSize);
}

int lcs_iterative() {
    unsigned int aPos, bPos;

    for (aPos = 1; aPos < (aSize + 1); aPos++) {
        for (bPos = 1; bPos < (bSize + 1); bPos++) {
            if (aString[aPos - 1] == bString[bPos - 1]) {
                setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
            } else {
                setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), getResultMatrix(aPos, bPos - 1)));
            }
        }
    }

    return getResultMatrix(aSize, bSize);
}

int lcs_recursive(unsigned int aPos, unsigned int bPos) {
    if (aPos < 0 || bPos < 0 || aPos > aSize || bPos > bSize) {
        /* one should never access cells that are outside of the matrix's bounds */
        return -1;
    }

    if (getResultMatrix(aPos, bPos) != -1) {
        /* previously calculated values need only to be returned */
        return getResultMatrix(aPos, bPos);
    }

    /* if the values hasn't been calculated yet, do so, store in resultMatrix, and return that value */
    if(aPos == 0 || bPos == 0) {
        setResultMatrix(aPos, bPos, 0);
        return 0;
    }

    if(aPos > 0 && bPos > 0 && aString[aPos - 1] == bString[bPos - 1]) {
        setResultMatrix(aPos, bPos, lcs_recursive(aPos - 1, bPos - 1) + cost(aPos));
        return getResultMatrix(aPos, bPos);
    }

    if(aPos > 0 && bPos > 0 && aString[aPos - 1] != bString[bPos - 1]) {
        setResultMatrix(aPos, bPos, maxNumber(lcs_recursive(aPos, bPos - 1), lcs_recursive(aPos - 1, bPos)));
        return getResultMatrix(aPos, bPos);
    }

    /* this line should never be reached... but just in case */
    return -1;
}

void longest_sequence(int sequenceSize) {
    unsigned int aPos = aSize, bPos = bSize;
    while(aPos > 0 && bPos > 0) {
        if(aString[aPos - 1] == bString[bPos - 1]) {
            aPos--;
            bPos--;
            sequence[--sequenceSize] = aString[aPos];
        }
        else if(getResultMatrix(aPos, bPos - 1) >= getResultMatrix(aPos - 1, bPos)) {
            bPos--;
        }
        else {
            aPos--;
        }
    }
    return;
}

int lcs_algorithm() {
    int lcs_size, i;

    #if DEBUG
        fprintf(stderr, "ALGORITHM DEBUG INFO:\n");
    #endif

    /* find the size of the sequence, using the chosen implementation */
    #if RECURSIVE
        #if DEBUG
            fprintf(stderr, "\tRECURSIVE\n");
        #endif
        lcs_size = lcs_recursive(aSize, bSize);
    #elif ITERATIVE
        #if DEBUG
            fprintf(stderr, "\tITERATIVE\n");
        #endif
        lcs_size = lcs_iterative();
    #elif ANTI_DIAG
        #if DEBUG
            fprintf(stderr, "\tANTI DIAGONAL\n");
        #endif
        lcs_size = lcs_anti_diagonal();
    #else
        fprintf(stderr, "ERROR: no algorithm implementation was chosen.\n");
        return -1;
    #endif

    /* find the longest common sequence, and store it in sequence */
    sequence = (char *) malloc((lcs_size + 1) * sizeof(char));
    sequence[lcs_size] = '\0';
    longest_sequence(lcs_size);

    #if DEBUG
        fprintf(stderr, "END ALGORITHM DEBUG INFO.\n");
    #endif

    return lcs_size;
}


int main(int argc, char **argv)
{
    int sequenceSize;
    FILE *fp;

    #if TIME
        double start, end;
        start = omp_get_wtime();
    #endif

    #if DEBUG
        fprintf(stderr, "INIT DEBUG INFO:\n");
    #endif

    /* read from input */
    fp = fopen(argv[1],"r"); // read mode

    if( fp == NULL )
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d %d\n", &aSize, &bSize);

    /* allocate memory for the strings and the result matrix */
    initResultMatrix();
    aString = (char *) malloc((aSize + 1) * sizeof(char));
    bString = (char *) malloc((bSize + 1) * sizeof(char));

    /* get string A */
    saveString(aString, fp, aSize);

    /* get string B */
    saveString(bString, fp, bSize);

    /* close input */
    fclose(fp);

    #if DEBUG
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        //printResultMatrix(stderr);
        //fprintf(stderr, "\t%s\n", aString);
        //fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END INIT DEBUG INFO.\n");
    #endif

    /* find the largest sequence, and its size */
    sequenceSize = lcs_algorithm();

    if(sequenceSize == -1) {
        exit(EXIT_FAILURE);
    }

    /* output results to stdout */
    printf("%d\n", sequenceSize);
    printf("%s\n", sequence);

    #if DEBUG
        fprintf(stderr, "FINISH DEBUG INFO:\n");
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        //printResultMatrix(stderr);
        //fprintf(stderr, "\t%s\n", aString);
        //fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END FINISH DEBUG INFO.\n");
    #endif

    /* free used memory */
    freeResultMatrix();
    free(aString);
    free(bString);
    free(sequence);

    #if TIME
        end = omp_get_wtime();
        fprintf(stderr, "TIME INFO:\n");
        fprintf(stderr, "\tTime start: %fs\n", start);
        fprintf(stderr, "\tTime end: %fs\n", end);
        fprintf(stderr, "\tTime tick: %fs\n", omp_get_wtick());
        fprintf(stderr, "\tElapsed time: %fs\n", end - start);
        fprintf(stderr, "END TIME INFO.\n");
    #endif

    /* return */
    return 0;
}
