#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#define NUM_THREADS 4
#define LOCK_SIZE 100

#define RECURSIVE 0
#define ITERATIVE 1
#define ANTI_DIAG 0

#define HIGH_COST 1

#define DEBUG 0
#define TIME 0

char *aString, *bString, *sequence;
short *resultMatrix;
omp_lock_t *resultMatrixLocks;
int aSize, bSize, locksPerLine;

short maxNumber(short a, short b) {
    if(a >= b) {
        return a;
    }

    return b;
}

#if HIGH_COST
/* used to make this problem more computationally difficult than it really is *
 * call this using a variable, and not a constant!                            */
short cost(int x) {
    int i, n_iter = 20;
    double dcost = 0;
    for(i = 0; i < n_iter; i++)
        dcost += pow(sin((double) x),2) + pow(cos((double) x),2);
    return (short) (dcost / n_iter + 0.1);
}
#else
short cost(int x) {
    return 1;
}
#endif


void saveString(char *destString, FILE *fp, int size) {
    int i;

    for(i = 0; i < size; i++) {
        fscanf(fp, "%c", &(destString[i]));
    }
    fscanf(fp, "\n");

    /* terminate string with \0 */
    destString[size] = '\0';
}

void printResultMatrix(FILE *stream) {
    unsigned int i, j;

    #pragma omp critical(PRINT_MATRIX)
    {
        for(i = 0; i < (aSize + 1); i++) {
            fprintf(stream, "\t");
            for(j = 0; j < (bSize + 1); j++) {
                fprintf(stream, "%3d|", resultMatrix[i * (bSize + 1) + j]);
            }
            fprintf(stream, "\n");
        }
    }
}

void initResultMatrix() {
    unsigned int i, j;
    /* allocate memory to the result matrix */
    /* resultMatrix has one extra line and one extra column */
    resultMatrix = (short *) malloc ((aSize + 1) * (bSize + 1) * sizeof(short));

    #if LOCK_SIZE
        /* allocate memory to the locks */
        locksPerLine = (bSize + 1) / LOCK_SIZE; /* one lock per LOCK_SIZE elements */
        if ((bSize + 1) % LOCK_SIZE) { /* plus one lock for the rest */
            locksPerLine++;
        }
        resultMatrixLocks = (omp_lock_t *) malloc ((aSize + 1) * locksPerLine * sizeof(omp_lock_t));
    #endif

    #pragma omp parallel private(i, j) shared(resultMatrix) //if problem is sufficiently big?
    {
		/* Initialize first row and column of resultMatrix with 0 */
		#pragma omp for nowait
		for (j = 0; j < (bSize + 1); j++) {
			resultMatrix[j] = 0;
		}
        
        #if LOCK_SIZE
            /* Initialize first row of locks */
            #pragma omp for nowait
            for(j = 0; j < locksPerLine; j++) {
                omp_init_lock(&(resultMatrixLocks[j]));
            }
        #endif

		#pragma omp for nowait
		for (i = 1; i < (aSize + 1); i++) {
			resultMatrix[i * (bSize + 1)] = 0;
            
            #if LOCK_SIZE
                /* Initialize first column of locks */
                omp_init_lock(&(resultMatrixLocks[i * locksPerLine]));
                
                #if LOCK_SIZE > 1
                    /* lock for reading - should only read once ALL values protected by lock are written */
					omp_set_lock(&(resultMatrixLocks[i * locksPerLine]));
				#endif
            #endif
		}
		
        /* Initialize all other cells with -1, meaning it has not yet been calculated */
        #pragma omp for nowait
        for(i = 1; i < (aSize + 1); i++) {        
            for(j = 1; j < (bSize + 1); j++) {
                resultMatrix[i * (bSize + 1) + j] = -1;
            }
            
            #if LOCK_SIZE
                /* Initialize all other locks and lock them */
                for(j = 1; j < locksPerLine; j++) {
                    omp_init_lock(&(resultMatrixLocks[i * locksPerLine + j]));

                    /* lock for reading - should only read once all values protected by lock are written */
                    omp_set_lock(&(resultMatrixLocks[i * locksPerLine + j]));
                }
            #endif
        }
	}


}

short getResultMatrix(unsigned int aPos, unsigned int bPos) {
    int result;

    #if DEBUG
        if(aPos > aSize || bPos > bSize) {
            fprintf(stderr, "\tILLEGAL ACCESS: reading (%d %d)\n", aPos, bPos);
        }
    #endif

    #if LOCK_SIZE
        omp_set_lock(&(resultMatrixLocks[aPos * locksPerLine + (bPos / LOCK_SIZE)]));
        result = resultMatrix[aPos * (bSize + 1) + bPos];
        omp_unset_lock(&(resultMatrixLocks[aPos * locksPerLine + (bPos / LOCK_SIZE)]));
    #else
        result = resultMatrix[aPos * (bSize + 1) + bPos];
    #endif

    return result;
}

void setResultMatrix(unsigned int aPos, unsigned int bPos, short value) {
    #if DEBUG
        if(aPos > aSize || bPos > bSize) {
            fprintf(stderr, "\tILLEGAL ACCESS: writting %d at (%d %d)\n", value, aPos, bPos);
        }
    #endif

    resultMatrix[aPos * (bSize + 1) + bPos] = value;

    #if LOCK_SIZE
        /* if it has calculated all values guarded by the lock, unlock it */
        if((bPos + 1) % LOCK_SIZE == 0 || bPos == bSize) {
            omp_unset_lock(&(resultMatrixLocks[aPos * locksPerLine + (bPos / LOCK_SIZE)]));

            #if DEBUG
                //fprintf(stderr, "UNLOCKNG [%d %d] for reaching (%d %d)\n", aPos, (bPos / LOCK_SIZE), aPos, bPos);
                //fflush(stderr);
            #endif
        }
    #endif
}

void freeResultMatrix() {
    #if LOCK_SIZE
        int i;

        for(i = 0; i < (aSize + 1) * locksPerLine; i++) {
            omp_destroy_lock(&(resultMatrixLocks[i]));
        }
        free(resultMatrixLocks);
    #endif

    free(resultMatrix);
}

int lcs_anti_diagonal() {
    int aPos, bPos, aStart = 1, bStart = 1, i;
    int corner = 0;

    while(bStart < (bSize + 1)) {

        if (!corner) { /* vai bater no topo da matriz */

            #pragma omp parallel for private(i, aPos, bPos)
            for(i = 0; i < aStart; i++) {

                aPos = aStart - i;
                bPos = bStart + i;

                if (aString[aPos - 1] == bString[bPos - 1]) {
                    setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
                } else {
                    setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), getResultMatrix(aPos, bPos - 1)));
                }

                if (aPos == 1 && bPos == bSize) { /* chegamos ao canto? */
                    corner = 1;
                    #if DEBUG
                        fprintf(stderr, "\tCorner reached!\n");
                    #endif
                }
            }

        } else { /* vai bater no lado da matriz */

            #pragma omp parallel for private(i, aPos, bPos)
            for(i = 0; i < (bSize - bStart + 1); i++) {

                aPos = aStart - i;
                bPos = bStart + i;

                if (aString[aPos - 1] == bString[bPos - 1]) {
                    setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
                } else {
                    setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), getResultMatrix(aPos, bPos - 1)));
                }
            }
        }


        if(aStart == aSize) {
            bStart++;
        } else {
            aStart++;
        }
    }

    return getResultMatrix(aSize, bSize);
}

int lcs_iterative() {
    unsigned int aPos, bPos;

    #pragma omp parallel for private(aPos, bPos) shared(resultMatrix) schedule(dynamic, 1)
    for (aPos = 1; aPos < (aSize + 1); aPos++) {
        for (bPos = 1; bPos < (bSize + 1); bPos++) {
            if (aString[aPos - 1] == bString[bPos - 1]) {
                setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
            } else {
				/* since accessing the position directly to the left is always safe, we can access it directly 	 */
				/* doing so avoids deadlocks where a thread cannot read said cell								 * 
				 * because it has not yet reached the end of the block guarded by the lock that guards said cell */
                setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), resultMatrix[aPos * (bSize + 1) + (bPos - 1)]));
            }
        }
    }

    return getResultMatrix(aSize, bSize);
}

int lcs_recursive(unsigned int aPos, unsigned int bPos) {
    if (aPos < 0 || bPos < 0 || aPos > aSize || bPos > bSize) {
        /* one should never access cells that are outside of the matrix's bounds */
        return -1;
    }

    if (getResultMatrix(aPos, bPos) != -1) {
        /* previously calculated values need only to be returned */
        return getResultMatrix(aPos, bPos);
    }

    /* if the values hasn't been calculated yet, do so, store in resultMatrix, and return that value */
    if(aPos == 0 || bPos == 0) {
        setResultMatrix(aPos, bPos, 0);
        return 0;
    }

    if(aPos > 0 && bPos > 0 && aString[aPos - 1] == bString[bPos - 1]) {
        setResultMatrix(aPos, bPos, lcs_recursive(aPos - 1, bPos - 1) + cost(aPos));
        return getResultMatrix(aPos, bPos);
    }

    if(aPos > 0 && bPos > 0 && aString[aPos - 1] != bString[bPos - 1]) {
        setResultMatrix(aPos, bPos, maxNumber(lcs_recursive(aPos, bPos - 1), lcs_recursive(aPos - 1, bPos)));
        return getResultMatrix(aPos, bPos);
    }

    /* this line should never be reached... but just in case */
    return -1;
}

void longest_sequence(int sequenceSize) {
    unsigned int aPos = aSize, bPos = bSize;
    while(aPos > 0 && bPos > 0) {
        if(aString[aPos - 1] == bString[bPos - 1]) {
            aPos--;
            bPos--;
            sequence[--sequenceSize] = aString[aPos];
        }
        else if(getResultMatrix(aPos, bPos - 1) >= getResultMatrix(aPos - 1, bPos)) {
            bPos--;
        }
        else {
            aPos--;
        }
    }
    return;
}

int lcs_algorithm() {
    int lcs_size, i;

    #if DEBUG
        fprintf(stderr, "ALGORITHM DEBUG INFO:\n");
    #endif

    /* find the size of the sequence, using the chosen implementation */
    #if RECURSIVE
        #if DEBUG
            fprintf(stderr, "\tRECURSIVE\n");
        #endif
        lcs_size = lcs_recursive(aSize, bSize);
    #elif ITERATIVE
        #if DEBUG
            fprintf(stderr, "\tITERATIVE\n");
        #endif
        lcs_size = lcs_iterative();
    #elif ANTI_DIAG
        #if DEBUG
            fprintf(stderr, "\tANTI DIAGONAL\n");
        #endif
        lcs_size = lcs_anti_diagonal();
    #else
        fprintf(stderr, "ERROR: no algorithm implementation was chosen.\n");
        return -1;
    #endif

    /* find the longest common sequence, and store it in sequence */
    sequence = (char *) malloc((lcs_size + 1) * sizeof(char));
    sequence[lcs_size] = '\0';
    longest_sequence(lcs_size);

    #if DEBUG
        fprintf(stderr, "END ALGORITHM DEBUG INFO.\n");
    #endif

    return lcs_size;
}


int main(int argc, char **argv)
{
    int sequenceSize;
    FILE *fp;

    #if TIME
        double start, end;
        start = omp_get_wtime();
    #endif

    /* set number of threads? */
    //omp_set_num_threads(NUM_THREADS);
    #if DEBUG
        fprintf(stderr, "INIT DEBUG INFO:\n");
        fprintf(stderr, "\tMax Number of Threads: %d\n", omp_get_max_threads());
    #endif

    /* read from input */
    fp = fopen(argv[1],"r"); // read mode

    if( fp == NULL )
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d %d\n", &aSize, &bSize);

    /* allocate memory for the strings and the result matrix */
    initResultMatrix();
    aString = (char *) malloc((aSize + 1) * sizeof(char));
    bString = (char *) malloc((bSize + 1) * sizeof(char));

    /* get string A */
    saveString(aString, fp, aSize);

    /* get string B */
    saveString(bString, fp, bSize);

    /* close input */
    fclose(fp);

    #if DEBUG
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        //printResultMatrix(stderr);
        //fprintf(stderr, "\t%s\n", aString);
        //fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END INIT DEBUG INFO.\n");
    #endif

    /* find the largest sequence, and its size */
    sequenceSize = lcs_algorithm();

    if(sequenceSize == -1) {
        exit(EXIT_FAILURE);
    }

    /* output results to stdout */
    printf("%d\n", sequenceSize);
    printf("%s\n", sequence);

    #if DEBUG
        fprintf(stderr, "FINISH DEBUG INFO:\n");
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        //printResultMatrix(stderr);
        //fprintf(stderr, "\t%s\n", aString);
        //fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END FINISH DEBUG INFO.\n");
    #endif

    /* free used memory */
    freeResultMatrix();
    free(aString);
    free(bString);
    free(sequence);

    #if TIME
        end = omp_get_wtime();
        fprintf(stderr, "TIME INFO:\n");
        fprintf(stderr, "\tTime start: %fs\n", start);
        fprintf(stderr, "\tTime end: %fs\n", end);
        fprintf(stderr, "\tTime tick: %fs\n", omp_get_wtick());
        fprintf(stderr, "\tElapsed time: %fs\n", end - start);
        fprintf(stderr, "END TIME INFO.\n");
    #endif

    /* return */
    return 0;
}
