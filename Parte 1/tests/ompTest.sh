#!/bin/bash

if [ ! -d debug ]; then
    echo "Making debug folder" ;
    mkdir debug ;
else
    echo "Cleaning debug directory of parallel debug info"
    rm -f omp.*.debug ;
fi

echo "Testing parallel implementation..." ;

for x in *.in; do
    echo -e "$x" ;
    ../build/lcs-omp $x > omp.${x%.in}.outhyp 2> ./debug/omp.${x%.in}.debug ;
    
    diff -B -w ${x%.in}.out omp.${x%.in}.outhyp > omp.${x%.in}.diff ;
    if [ -s omp.${x%.in}.diff ]; then
        echo "FAIL: $x" ;
    else
        rm -f omp.${x%.in}.diff omp.${x%.in}.outhyp ; 
    fi

    if [ ! -s ./debug/omp.${x%.in}.debug ]; then
        rm -f ./debug/omp.${x%.in}.debug ; 
    fi
done