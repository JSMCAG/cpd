#!/bin/bash

if [ ! -d debug ]; then
    echo "Making debug folder" ;
    mkdir debug ;
else
    echo "Cleaning debug directory of serial and parallel debug info"
    rm -f serial.*.debug ;
    rm -f omp.*.debug ;
fi

echo "Comparing serial and parallel implementations..." ;

for x in *.in; do
    echo -e "Parallel $x" ;
    ../build/lcs-omp $x  2> ./debug/omp.${x%.in}.debug ;

    echo -e "Serial $x" ;
    ../build/lcs-serial $x  2> ./debug/serial.${x%.in}.debug ;

    if [ ! -s ./debug/omp.${x%.in}.debug ]; then
        rm -f ./debug/omp.${x%.in}.debug ; 
    fi
    if [ ! -s ./debug/serial.${x%.in}.debug ]; then
        rm -f ./debug/serial.${x%.in}.debug ; 
    fi
done