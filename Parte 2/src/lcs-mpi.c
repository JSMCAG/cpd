#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define BLOCK_SIZE 1000

#define HIGH_COST 1

#define DEBUG 0
#define TIME 0

char *aString, *bString, *sequence;
//short *resultMatrix;
short *resultRows;
int aSize, bSize, id, p, id_with_seq, n_rows, lcs_size;

short *prevRowBlockValues;
int prevRowBlockColStart, prevRowBlockRowNum;
MPI_Request request;
short isFirstBlockSent = 0;

short maxNumber(short a, short b) {
    if(a >= b) {
        return a;
    }

    return b;
}

int minNumber(int a, int b) {
    if(a <= b) {
        return a;
    }

    return b;
}

#if HIGH_COST
/* used to make this problem more computationally difficult than it really is *
 * call this using a variable, and not a constant!                            */
short cost(int x) {
    int i, n_iter = 20;
    double dcost = 0;
    for(i = 0; i < n_iter; i++)
        dcost += pow(sin((double) x),2) + pow(cos((double) x),2);
    return (short) (dcost / n_iter + 0.1);
}
#else
short cost(int x) {
    return 1;
}
#endif


void saveString(char *destString, FILE *fp, int size) {
    int i;

    for(i = 0; i < size; i++) {
        fscanf(fp, "%c", &(destString[i]));
    }
    fscanf(fp, "\n");

    /* terminate string with \0 */
    destString[size] = '\0';
}

void printResultRows(FILE *stream) {
    unsigned int i, j;
    for(i = 0; i < n_rows; i++) {
        fprintf(stream, "\t");
        for(j = 0; j < (bSize + 1); j++) {
            fprintf(stream, "%3d|", resultRows[i * (bSize + 1) + j]);
        }
        fprintf(stream, "\n");
    }
}

void initResultRows() {
    unsigned int i, j;
    /* allocate memory to the rows of result matrix this node owns  */
    /* the allocation follows a round-robin style, starting at 1    */
    /* (row 0 is ommited, for better load balancing)                */
    n_rows = aSize / p;
    if (((aSize % p) > 0) && (id < (aSize % p))) {
        n_rows++;
    }
    resultRows = (short *) malloc(n_rows * (bSize + 1) * sizeof(short));


    /* Initialize first column of resultRows with 0 */
    for (i = 0; i < n_rows; i++) {
        resultRows[i * (bSize + 1)] = 0;
    }
    /* Initialize all other cells with -1, meaning it has not yet been calculated */
    for(i = 0; i < n_rows; i++) {
        for(j = 1; j < (bSize + 1); j++) {
            resultRows[i * (bSize + 1) + j] = -1;
        }
    }

    /* allocate memory to contain the blocks recieved */
    prevRowBlockValues = (short *) malloc(BLOCK_SIZE * sizeof(short));
    prevRowBlockColStart = -1;
}

short whoOwnsMatrixRow(unsigned int row) {
    if (row) {
        return (row - 1) % p;
    }

    return 0;
}

short getValueOfResultMatrix(unsigned int aPos, unsigned int bPos) {
    short value, rowOwner = whoOwnsMatrixRow(aPos);

    if (aPos == 0) {
        return 0;
    }
    
    if(rowOwner == id) {
        unsigned int row = ((aPos - 1) / p);
        value = resultRows[row * (bSize + 1) + bPos]; 
    }

    MPI_Bcast(&value, 1, MPI_SHORT, rowOwner, MPI_COMM_WORLD);

    return value;
}

short getResultMatrix(unsigned int aPos, unsigned int bPos) {
    /* while only a few rows are stored in each node, in order to       */
    /* facilitate the implementation of the algorithm, we will abstract */
    /* the reads and writes to the matrix as if it were still whole     */

    MPI_Status status;
    unsigned int row = ((aPos - 1) / p);
    unsigned int rowOwner = whoOwnsMatrixRow(aPos);

    if (aPos == 0) {
        /* the first row for the resultMatrix is actually not present in        */
        /* the first node's memory, in order to facilitate memory allocation    */
        /* and distribution. However, any access to that line returns 0         */
        return 0;
    }

    if (rowOwner != id) {
        /* if this node doesn't own the row it is trying to read, then it   */
        /* must check if it has recieved it before and stored it for later  */
        if( (prevRowBlockColStart >= 0) && 
            (bPos >= prevRowBlockColStart) && 
            (bPos < (prevRowBlockColStart + BLOCK_SIZE)) &&
            (aPos == prevRowBlockRowNum)) {
            /* if it has, then we only need to return that */
            return prevRowBlockValues[bPos - prevRowBlockColStart];
        }

        /* otherwise, it must wait for the values to be sent from the node previous to it */
        MPI_Recv(prevRowBlockValues, BLOCK_SIZE, MPI_SHORT, rowOwner, aPos, MPI_COMM_WORLD, &status);
        int startOfBlock = (bPos / BLOCK_SIZE) * BLOCK_SIZE;

        prevRowBlockColStart = startOfBlock;
        prevRowBlockRowNum = aPos;

        return prevRowBlockValues[bPos - prevRowBlockColStart];
    }

    /* if this node owns the row it is trying to access, then we only need to return the value */
    return resultRows[row * (bSize + 1) + bPos];
}

void setResultMatrix(unsigned int aPos, unsigned int bPos, short value) {
    unsigned int row = ((aPos - 1) / p);
    resultRows[row * (bSize + 1) + bPos] = value;

    /* if we have finished this block, and the next node needs it, we must send it to the next node */
    if(((bPos + 1) % BLOCK_SIZE == 0 || bPos == bSize) && (aPos < aSize)) {
        /* find the block to send */
        int startOfBlock = (bPos / BLOCK_SIZE) * BLOCK_SIZE; // yes, this actually works. Don't you just love integer divisions?
        int endOfBlock = minNumber(startOfBlock + BLOCK_SIZE - 1, bSize);
        short* block = &(resultRows[row * (bSize + 1) + startOfBlock]);
        
        int sizeOfBlock = endOfBlock - startOfBlock + 1;

        /* if a send was made before, wait until last block was recieved */
        if(isFirstBlockSent) {
            MPI_Status status;
            MPI_Wait(&request, &status);
        }

        /* send block to next node */
        MPI_Isend(block, sizeOfBlock, MPI_SHORT, (id + 1) % p, aPos, MPI_COMM_WORLD, &request);
        isFirstBlockSent = 1;
    }
}

void freeResultMatrix() {
    free(resultRows);
    free(prevRowBlockValues);
}

int lcs_iterative() {
    unsigned int aPos, bPos;

    for (aPos = id + 1; aPos < (aSize + 1); aPos += p) {
        for (bPos = 1; bPos < (bSize + 1); bPos++) {
            if (aString[aPos - 1] == bString[bPos - 1]) {
                setResultMatrix(aPos, bPos, getResultMatrix(aPos - 1, bPos - 1) + cost(aPos));
            } else {
                setResultMatrix(aPos, bPos, maxNumber(getResultMatrix(aPos - 1, bPos), getResultMatrix(aPos, bPos - 1)));
            }
        }
    }

    lcs_size = getValueOfResultMatrix(aSize, bSize);
    return lcs_size;
}

void longest_sequence(int sequenceSize) {
    unsigned int aPos = aSize, bPos = bSize;
    int header[3];
    MPI_Status status;
    
    int recv_from = (id + 1) % p;
    int send_to = (id - 1) % p;
    if (send_to < 0) {
        send_to += p;
    }

    while(1) {
        if(whoOwnsMatrixRow(aPos) == id) {
            if(aPos == 0 || bPos == 0) {
                /* execution is finished! Time to send STOP message */
                header[0] = -1;
                header[1] = id;

                MPI_Send(header, 3, MPI_INT, send_to, 1, MPI_COMM_WORLD);
        
                /* I am the one with the sequence! */
                id_with_seq = id;

                /* and get out  */
                break;
            }
            
            if(aString[aPos - 1] == bString[bPos - 1]) {
                aPos--;
                bPos--;
                sequence[--sequenceSize] = aString[aPos];

                /* we can no longer work on this processor - pass the job up    */
                header[0] = aPos;
                header[1] = bPos;
                header[2] = sequenceSize;
        
                MPI_Send(header, 3, MPI_INT, send_to, 1, MPI_COMM_WORLD);
                MPI_Send(sequence, lcs_size + 1, MPI_CHAR, send_to, 1, MPI_COMM_WORLD);
            }
            else if(getResultMatrix(aPos, bPos) > getResultMatrix(aPos, bPos - 1)) {
                aPos--;

                /* we can no longer work on this processor - pass the job up    */
                header[0] = aPos;
                header[1] = bPos;
                header[2] = sequenceSize;
        
                MPI_Send(header, 3, MPI_INT, send_to, 1, MPI_COMM_WORLD);
                MPI_Send(sequence, lcs_size + 1, MPI_CHAR, send_to, 1, MPI_COMM_WORLD);
            }
            else {
                bPos--;
            }
        } else {
            MPI_Recv(header, 3, MPI_INT, (id + 1) % p, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            /* header[0] is aPos if not done, -1 if done (STOP) */
            if(header[0] < 0) {
                /* if done, header[1] is the id of the node who finished    */
                id_with_seq = header[1];
                if(header[1] != ((id - 1) % p)) {
                    /* should only send STOP message if the previous node is not the one who finished it    */
                    MPI_Send(header, 3, MPI_INT, send_to, 1, MPI_COMM_WORLD);
                }
                break;
            }
            MPI_Recv(sequence, lcs_size + 1, MPI_CHAR, (id + 1) % p, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

            aPos = header[0];
            bPos = header[1];
            sequenceSize = header[2];
        }
    }
    return;
}

int lcs_algorithm() {
    /* find the size of the sequence */
    lcs_iterative();

    /* find the longest common sequence, and store it in sequence */
    sequence = (char *) malloc((lcs_size + 1) * sizeof(char));
    sequence[lcs_size] = '\0';
    longest_sequence(lcs_size);

    return lcs_size;
}


int main(int argc, char **argv)
{
    FILE *fp;

    #if TIME
        double start, end;
        start = MPI_Wtime();
    #endif
    
    /* read from input */
    fp = fopen(argv[1],"r"); // read mode

    if( fp == NULL )
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }

    fscanf(fp, "%d %d\n", &aSize, &bSize);
    
    /* load strings */
    aString = (char *) malloc((aSize + 1) * sizeof(char));
    bString = (char *) malloc((bSize + 1) * sizeof(char));

    /* get string A */
    saveString(aString, fp, aSize);

    /* get string B */
    saveString(bString, fp, bSize);

    /* close input */
    fclose(fp);
    
    
    /* start processes */
    MPI_Init(&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &id);
    MPI_Comm_size (MPI_COMM_WORLD, &p);
    
    /* share aString and bString with all processes */
    MPI_Bcast(aString, aSize + 1, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(bString, bSize + 1, MPI_CHAR, 0, MPI_COMM_WORLD);
    
    MPI_Bcast(&aSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&bSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    /* allocate memory for the result matrix */
    initResultRows();


    #if DEBUG
        int token = 1;
        if(id != 0) {
            MPI_Status s;
            MPI_Recv(&token, 1, MPI_INT, (id - 1), (id - 1), MPI_COMM_WORLD, &s);
        }
        fprintf(stderr, "INIT DEBUG INFO:\n");
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        fprintf(stderr, "\tProcess id = %d\n", id);
        printResultRows(stderr);
        fprintf(stderr, "\t%s\n", aString);
        fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END INIT DEBUG INFO.\n");
        if ((id + 1) != p) {
            MPI_Send(&token, 1, MPI_INT, (id + 1), id, MPI_COMM_WORLD);
        }
    #endif

    /* find the largest sequence, and its size */
    lcs_algorithm();

    /* output results to stdout */
    if (id_with_seq == id) {
        printf("%d\n", lcs_size);
        printf("%s\n", sequence);
    }

    #if DEBUG
        if(id != 0) {
            MPI_Status s;
            MPI_Recv(&token, 1, MPI_INT, (id - 1), (id - 1), MPI_COMM_WORLD, &s);
        }
        fprintf(stderr, "FINISH DEBUG INFO:\n");
        /* Due to the large amount of info that can be printed here,  *
         * be sure to only uncomment this IF YOU ARE SURE YOU WANT IT */
        fprintf(stderr, "\tProcess id = %d\n", id);
        printResultRows(stderr);
        fprintf(stderr, "\t%s\n", aString);
        fprintf(stderr, "\t%s\n", bString);
        fprintf(stderr, "END FINISH DEBUG INFO.\n");
        if ((id + 1) != p) {
            MPI_Send(&token, 1, MPI_INT, (id + 1), id, MPI_COMM_WORLD);
        }
    #endif

    /* free used memory */
    freeResultMatrix();
    free(aString);
    free(bString);
    free(sequence);

    #if TIME
        end = MPI_Wtime();
        fprintf(stderr, "TIME INFO:\n");
        fprintf(stderr, "\tTime start: %fs\n", start);
        fprintf(stderr, "\tTime end: %fs\n", end);
        fprintf(stderr, "\tTime tick: %fs\n", MPI_Wtick());
        fprintf(stderr, "\tElapsed time: %fs\n", end - start);
        fprintf(stderr, "END TIME INFO.\n");
    #endif

    /* return */
    MPI_Finalize();
    return 0;
}
