import random
import string

aSize = input("aSize = ")
bSize = input("bSize = ")

letters = ['A', 'G', 'T', 'C']

aString = ''.join(random.SystemRandom().choice(letters) for _ in range(aSize))
bString = ''.join(random.SystemRandom().choice(letters) for _ in range(bSize))

file = open("test.in", "w")
file.write(str(aSize) + " " + str(bSize) + "\n")
file.write(aString + "\n")
file.write(bString)
file.close()

print("Done!")
