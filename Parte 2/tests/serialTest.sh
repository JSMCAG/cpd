#!/bin/bash

if [ ! -d debug ]; then
    echo "Making debug folder" ;
    mkdir debug ;
else
    echo "Cleaning debug directory of serial debug info"
    rm -f serial.*.debug ;
fi

echo "Testing serial implementation..." ;

for x in *.in; do
    echo -e "$x" ;
    ../build/lcs-serial $x > serial.${x%.in}.outhyp 2> ./debug/serial.${x%.in}.debug ;
    
    diff -B -w ${x%.in}.out serial.${x%.in}.outhyp > serial.${x%.in}.diff ;
    if [ -s serial.${x%.in}.diff ]; then
        echo "FAIL: $x" ;
    else
        rm -f serial.${x%.in}.diff serial.${x%.in}.outhyp ; 
    fi

    if [ ! -s ./debug/serial.${x%.in}.debug ]; then
        rm -f ./debug/serial.${x%.in}.debug ; 
    fi
done